from tkinter import *
import cx_Oracle


queryBuscarJogador = r"""SELECT T.NOME, T.LOGOTIPO, C.CAMPEONATO, C.COLOCACAO
			FROM TIME T LEFT JOIN PARTICIPA_CAMPEONATO C
				ON T.NUM_INSCRICAO = C.TIME
				WHERE T.NOME = 'PALAVRA_CHAVE'
			ORDER BY T.NOME;"""
			
			
#-- BUSCA PELAS INFORMAÇÕES DE UM TIME
queryBuscarTime = r"""
SELECT T.NOME, T.LOGOTIPO, C.CAMPEONATO, C.COLOCACAO
	FROM TIME T LEFT JOIN PARTICIPA_CAMPEONATO C
	ON T.NUM_INSCRICAO = C.TIME
WHERE T.NOME = 'PALAVRA_CHAVE'
ORDER BY T.NOM;E"""

#-- BUSCAR TODOS OS TIMES DE UM CAMPEONATO
queryBuscarTodosTimes = r"""SELECT C.NOME, C.DATA, C.PREMIO_TOTAL, C.JOGO, PARTICIPA.COLOCACAO ,TIME.NOME, TIME.LOGOTIPO
	FROM CAMPEONATO C LEFT JOIN PARTICIPA_CAMPEONATO PARTICIPA
	ON C.NOME = PARTICIPA.CAMPEONATO
		LEFT JOIN TIME
		ON PARTICIPA.TIME = T.NUM_INSCRICAO
WHERE C.NOME = 'PALAVRA_CHAVE'
ORDER BY TIME.NOME;"""

#--BUSCAR A CHAVE DE UM CAMPEONATO PELO NOME DO CAMPEONATO
queryBuscarChave = r"""SELECT T.NOME, P.DATA_HORA, P.FASE, P.LOCAL, P.VITORIOSO
	FROM CAMPEONATO C JOIN PARTIDA P
	ON C.NOME = P.CAMPEONATO
		JOIN TIME T
		ON P.TIME1 = T.NUM_INSCRICAO OR P.TIME2 = T.NUM_INSCRICAO
WHERE C.NOME = 'PALAVRA_CHAVE'
ORDER BY P.FASE;"""


#--BUSCAR AS PROXIMAS PARTIDAS
queryProximasPartidas = r"""SELECT T.NOME, P.DATA_HORA, P.FASE, P.LOCAL
	FROM PARTIDA P JOIN TIME T
	ON P.TIME1 = T.NUM_INSCRICAO OR P.TIME2 = T.NUM_INSCRICAO
WHERE SYSDATE < P.DATA_HORA	   
ORDER BY P.DATA_HORA"""


#--BUSCAR AS ESTATICAS DOS JOGADORES EM UMA DETERMINADA PARTIDA DE LOL
queryEstatisticaPartida = r"""SELECT PESSOA.NOME, T.NOME,S.KILLS,S.DEATHS,S.ASSISTS ,S.POSICAO
	FROM PARTIDA P JOIN TIME T
	ON P.TIME1 = T.NUM_INSCRICAO OR P.TIME2 = T.NUM_INSCRICAO
		JOIN JOGADOR J
		ON J.TIME_ATUAL = T.NUM_INSCRICAO
			JOIN STATUS_CS S
			ON S.JOGADOR = J.PESSOA AND S.PARTIDA = P.ID
					LEFT JOIN PESSOA PESSOA 
	    			ON PESSOA.CPF = J.PESSOA

WHERE (P.TIME1 = 'PALAVRA_CHAVE' AND P.TIME2 = 'PALAVRA_CHAVE_2' AND P.DATA_HORA = 'PALAVRA_CHAVE_3')
ORDER BY T.NOME,PESSOA.NOME"""


#--BUSCAR AS ESTATICAS DE UM JOGADOR ESPECIFICO EM UMA PARTIDA ESPECIFICA DE CS
queryEstaticaPartida = r"""SELECT PESSOA.NOMET.NOME,S.KILLS,S.DEATHS,S.ASSISTS,S.POSICAO
	FROM PARTIDA P JOIN TIME T
		ON P.TIME1 = T.NUM_INSCRICAO OR P.TIME2 = T.NUM_INSCRICAO
	JOIN JOGADOR J
		ON J.TIME_ATUAL = T.NUM_INSCRICAO
	JOIN STATUS_CS S
		ON S.JOGADOR = J.PESSOA AND S.PARTIDA = P.ID
        LEFT JOIN PESSOA P 
		ON PESSOA.CPF = J.PESSOA


WHERE (P.TIME1 = 'PALAVRA_CHAVE' AND P.TIME2 = 'PALAVRA_CHAVE_2' AND P.DATA_HORA = 'PALAVRA_CHAVE_3' AND T.NOME = 'PALAVRA_CHAVE_4' 
ORDER BY T.NOME, PESSOA.NOME"""





class query:
	def __init__(self):
		self.con = cx_Oracle.connect('pythonhol/welcome@127.0.0.1/orcl')
		
	def execute(self,chave,queryText):
		self.cur = self.con.cursor()
		queryText.replace("PALAVRA_CHAVE",chave)
		self.cur.execute(queryText)
		returnValue = cur.fetchall()
		return returnValue

  def execute3keys(self,chave,queryText):
		self.cur = self.con.cursor()
		queryText.replace("PALAVRA_CHAVE",chave[0])
		queryText.replace("PALAVRA_CHAVE_2",chave[1])
		queryText.replace("PALAVRA_CHAVE_3",chave[2])
		self.cur.execute(queryText)
		returnValue = cur.fetchall()
		return returnValue

  def execute4keys(self,chave,queryText):
		self.cur = self.con.cursor()
		queryText.replace("PALAVRA_CHAVE",chave[0])
		queryText.replace("PALAVRA_CHAVE_2",chave[1])
		queryText.replace("PALAVRA_CHAVE_3",chave[2])
		queryText.replace("PALAVRA_CHAVE_4",chave[3])
		self.cur.execute(queryText)
		returnValue = cur.fetchall()
		return returnValue
	
	def fimQuery(self):
		self.cur.close()
		self.con.close()




class TelaPrincipal:
    def __init__(self,master):
        self.master = master
        self.parametros = []
        self.teste = query()

#       Configurando Janela Principal       
        self.window_width = master.winfo_screenwidth()
        self.window_height = master.winfo_screenheight()
        self.tamanho = str(self.window_width) + "x" + str(self.window_height)
        self.master.title("GameXP - Campeonatos")
        self.master.iconbitmap(default='logo.ico')
        self.master.geometry(self.tamanho)
        self.master.state("zoomed")

#       Carregando imagens      
        self.imgInicio = PhotoImage(file='background2.ppm')
        self.imgLogo = PhotoImage(file="logo.ppm")
        self.imgBPesquisa = PhotoImage(file="bpesquisar.ppm")
        self.imgBPesquisaPurple = PhotoImage(file="bpesquisarPurple.ppm")
        self.imgBTorneios = PhotoImage(file = "btorneios.ppm")
        self.imgBVoltar = PhotoImage(file="voltar.ppm")
        self.imgBVoltarAzul = PhotoImage(file="voltarPurple.ppm")
        self.crowd = PhotoImage(file="crowd.ppm")
        self.imgCrowd = PhotoImage(file="crowdCortada.ppm")
        self.imgLogoXP = PhotoImage(file="logoXPmenorAzul.ppm")
        self.imgTelaMaior = PhotoImage(file="TelaMaior.ppm")
        self.imgTelaMedia = PhotoImage(file="TelaMedia.ppm")
        self.imgBotaoPadrao = PhotoImage(file='botao160x90.ppm')
        self.imgBotPesqTime = PhotoImage(file='pesquisarTimesPurple.ppm')
        self.imgBotPesqJogador = PhotoImage(file='pesquisarJogadoresPurple.ppm')
        self.imgBotPesqEstatistica = PhotoImage(file='pesquisarEstatisticasPurple.ppm')
        self.imgTelaPadrao = PhotoImage(file='Tela.ppm')
        self.imgTelaEspelhadaPadrao = PhotoImage(file='TelaEspelhadaPurple.ppm')
        self.imgTelaSuperiorPadrao = PhotoImage(file='TelaPesquisa.ppm')
        self.imgTelaSuperiorMenor = PhotoImage(file='TelaPesquisaMenor.ppm')
        self.imgBAdmin = PhotoImage(file='badmin.ppm')
        self.imgBackTorneio = PhotoImage(file='TorneioPlaceHolder.ppm')

#       Criando Tela inicial
        self.telaInicial = Frame(master)
        self.canvasInicial = Canvas(self.telaInicial,width=self.window_width,height=self.window_height)
        self.canvasInicial.pack(expand=YES, fill=BOTH)
        self.canvasInicial.create_image(0,0,image=self.imgInicio,anchor=NW)
        self.canvasInicial.create_text(self.window_width/1.85,self.window_height/12,anchor=N,text="GAME XP - Campeonatos",font=("verdana",30),fill="white")
        self.canvasInicial.create_image(self.window_width/3.5,self.window_height/12,anchor=NW,image=self.imgLogo)
        self.botaoInicio = Button(self.telaInicial,command=lambda:self.mudar(self.telaInicial,self.telaSelecao),relief="raised",bd=-1,bg="white",image=self.imgBotaoPadrao)
        self.botaoInicio.place(relx=0.5,rely=0.65,anchor="center")
        self.telaInicial.pack()

#       Criando Tela de Seleção
        self.telaSelecao = Frame(master)
        self.canvasSelecao = Canvas(self.telaSelecao,width=self.window_width,height=self.window_height)
        self.canvasSelecao.pack(expand=YES, fill=BOTH)
        self.canvasSelecao.create_image(0,0,image=self.imgTelaMedia,anchor=NW)
        self.canvasSelecao.create_image(50,50,image=self.imgLogoXP,anchor=NW)
        self.botPesq = Button(self.telaSelecao,command=lambda:self.mudar(self.telaSelecao,self.telaPesquisa),relief="raised",bd=-1,bg="white",image=self.imgBPesquisa)
        self.botPesq.place(relx=0.7,rely=0.2,anchor="center")
        self.botTorneios = Button(self.telaSelecao,command=lambda:self.mudar(self.telaSelecao,self.telaTorneios),relief="raised",bd=-1,bg="white",image=self.imgBTorneios)
        self.botTorneios.place(relx=0.7,rely=0.35,anchor="center")
        self.botAdmin = Button(self.telaSelecao,command=self.admin,relief="raised",bd=-1,bg="white",image=self.imgBAdmin)
        self.botAdmin.place(relx=0.7,rely=0.50,anchor="center")
        self.botVoltar = Button(self.telaSelecao,command=lambda:self.mudar(self.telaSelecao,self.telaInicial),relief="raised",bd=-1,bg="white",image=self.imgBVoltar)
        self.botVoltar.place(relx=0.90,rely=0.90,anchor="center")

#       Criando Tela Pesquisa
        self.telaPesquisa = Frame(master)
        self.canvasPesquisa = Canvas(self.telaPesquisa,width=self.window_width,height=self.window_height)
        self.canvasPesquisa.pack(expand=YES, fill=BOTH)
        self.canvasPesquisa.create_image(0,0,image=self.imgTelaEspelhadaPadrao,anchor=NW)
        self.canvasPesquisa.create_image(50,50,image=self.imgCrowd,anchor=NW)
        self.botPesquisaJogador = Button(self.telaPesquisa,command=lambda:self.mudar(self.telaPesquisa,self.telaJogadores),relief="raised",bd=-1,bg="#6c0090",image=self.imgBotPesqJogador)
        self.botPesquisaJogador.place(relx=0.90,rely=0.15,anchor="center")
        self.botPesquisaTime = Button(self.telaPesquisa,command=lambda:self.mudar(self.telaPesquisa,self.telaTimes),relief="raised",bd=-1,bg="#6c0090",image=self.imgBotPesqTime)
        self.botPesquisaTime.place(relx=0.90,rely=0.30,anchor="center")
        self.botPesquisaEstatistica = Button(self.telaPesquisa,command=lambda:self.mudar(self.telaPesquisa,self.telaEstatistica),relief="raised",bd=-1,bg="#6c0090",image=self.imgBotPesqEstatistica)
        self.botPesquisaEstatistica.place(relx=0.90,rely=0.45,anchor="center")
        self.botVoltarPesquisa = Button(self.telaPesquisa,command=lambda:self.mudar(self.telaPesquisa,self.telaSelecao),relief="raised",bd=-1,bg="#6c0090",image=self.imgBVoltarAzul)
        self.botVoltarPesquisa.place(relx=0.90,rely=0.90,anchor="center")

#       Criando Tela de Torneios
        self.telaTorneios = Frame(master)
        self.canvasTorneios = Canvas(self.telaTorneios,width=self.window_width,height=self.window_height)
        self.canvasTorneios.pack(expand=YES, fill=BOTH)
        self.canvasTorneios.create_image(-50,0,image=self.imgBackTorneio,anchor=NW)
        self.botVoltarTorneios = Button(self.telaTorneios,command=lambda:self.mudar(self.telaTorneios,self.telaSelecao),relief="raised",bd=-1,bg="white",image=self.imgBVoltar)
        self.botVoltarTorneios.place(relx=0.90,rely=0.90,anchor="center")

#       Criando Tela de Busca Por Jogadores
        self.telaJogadores = Frame(master)
        self.canvasJogadores = Canvas(self.telaJogadores,width=self.window_width,height=self.window_height)
        self.canvasJogadores.pack(expand=YES, fill=BOTH)
        self.canvasJogadores.create_image(0,0,image=self.imgTelaPadrao,anchor=NW)
        self.canvasJogadores.create_text(self.window_width*0.3,self.window_height*0.15,text="Nome do Jogador",font=("verdana",30),fill="white")
        self.caixaTexto = Entry(self.telaJogadores,bg="#6c0090",text="Pesquise aqui",fg="white",font=("verdana",30),bd=5,highlightbackground="black")
        self.caixaTexto.place(relx=0.30,rely=0.25,anchor="center")
        self.botPesquisar = Button(self.telaJogadores,command=self.enviarPesquisa1(queryBuscarJogador),relief="raised",bd=-1,bg="#6c0090",image=self.imgBPesquisaPurple)
        self.botPesquisar.place(relx =0.30,rely=0.40,anchor="center")
        self.botVoltarJogadores = Button(self.telaJogadores,command=lambda:self.mudar(self.telaJogadores,self.telaPesquisa),relief="raised",bd=-1,bg="#6c0090",image=self.imgBVoltarAzul)
        self.botVoltarJogadores.place(relx=0.30,rely=0.90,anchor="center")

#       Criando Tela de Busca Por Times
        self.telaTimes = Frame(master)
        self.canvasTimes = Canvas(self.telaTimes,width=self.window_width,height=self.window_height)
        self.canvasTimes.pack(expand=YES, fill=BOTH)
        self.canvasTimes.create_image(0,0,image=self.imgTelaPadrao,anchor=NW)
        self.canvasTimes.create_text(self.window_width*0.3,self.window_height*0.15,text="Nome do Time",font=("verdana",30),fill="white")
        self.caixaTexto = Entry(self.telaTimes,bg="#6c0090",text="Pesquise aqui",fg="white",font=("verdana",30),bd=5,highlightbackground="black")
        self.caixaTexto.place(relx=0.30,rely=0.25,anchor="center")
        self.botPesquisar = Button(self.telaTimes,command=self.enviarPesquisa1(queryBuscarTime),relief="raised",bd=-1,bg="#6c0090",image=self.imgBPesquisaPurple)
        self.botPesquisar.place(relx=0.30,rely=0.40,anchor="center")
        self.botVoltarTimes = Button(self.telaTimes,command=lambda:self.mudar(self.telaTimes,self.telaPesquisa),relief="raised",bd=-1,bg="#6c0090",image=self.imgBVoltarAzul)
        self.botVoltarTimes.place(relx=0.30,rely=0.90,anchor="center")

#       Criando Tela de Busca Por Estatisticas
        self.telaEstatistica = Frame(master)
        self.canvasEstatistica = Canvas(self.telaEstatistica,width=self.window_width,height=self.window_height)
        self.canvasEstatistica.pack(expand=YES, fill=BOTH)
        self.canvasEstatistica.create_image(0,0,image=self.imgTelaPadrao,anchor=NW)
        self.canvasEstatistica.create_text(self.window_width*0.3,self.window_height*0.15,text="Nome Primeiro Time",font=("verdana",30),fill="white")
        self.time1 = Entry(self.telaEstatistica,bg="#6c0090",text="Pesquise aqui",fg="white",font=("verdana",30),bd=5,highlightbackground="black")
        self.time1.place(relx=0.30,rely=0.25,anchor="center")
        self.botPesquisar = Button(self.telaEstatistica,command=lambda:self.enviarPesquisa(self.time1,True,self.telaEstatistica,self.telaEstatistica1,False),relief="raised",bd=-1,bg="#6c0090",image=self.imgBPesquisaPurple)
        self.botPesquisar.place(relx=0.30,rely=0.80,anchor="center")
        self.botVoltarEstatisca = Button(self.telaEstatistica,command=lambda:self.mudar(self.telaEstatistica,self.telaPesquisa),relief="raised",bd=-1,bg="#6c0090",image=self.imgBVoltarAzul)
        self.botVoltarEstatisca.place(relx=0.30,rely=0.93,anchor="center")

#       Criando Estatistica 1 
        self.telaEstatistica1 = Frame(master)
        self.canvasEstatistica1 = Canvas(self.telaEstatistica1,width=self.window_width,height=self.window_height)
        self.canvasEstatistica1.pack(expand=YES, fill=BOTH)
        self.canvasEstatistica1.create_image(0,0,image=self.imgTelaPadrao,anchor=NW)
        self.canvasEstatistica1.create_text(self.window_width*0.3,self.window_height*0.15,text="Nome Segundo Time",font=("verdana",30),fill="white")
        self.time2 = Entry(self.telaEstatistica1,bg="#6c0090",text="Pesquise aqui",fg="white",font=("verdana",30),bd=5,highlightbackground="black")
        self.time2.place(relx=0.30,rely=0.25,anchor="center")
        self.botPesquisar = Button(self.telaEstatistica1,command=lambda:self.enviarPesquisa(self.time2,True,self.telaEstatistica1,self.telaEstatistica2,False),relief="raised",bd=-1,bg="#6c0090",image=self.imgBPesquisaPurple)
        self.botPesquisar.place(relx=0.30,rely=0.80,anchor="center")
        self.botVoltarEstatisca = Button(self.telaEstatistica1,command=lambda:self.mudar(self.telaEstatistica1,self.telaPesquisa),relief="raised",bd=-1,bg="#6c0090",image=self.imgBVoltarAzul)
        self.botVoltarEstatisca.place(relx=0.30,rely=0.93,anchor="center")

#       Criando estatistica 2
        self.telaEstatistica2 = Frame(master)
        self.canvasEstatistica2 = Canvas(self.telaEstatistica2,width=self.window_width,height=self.window_height)
        self.canvasEstatistica2.pack(expand=YES, fill=BOTH)
        self.canvasEstatistica2.create_image(0,0,image=self.imgTelaPadrao,anchor=NW)
        self.canvasEstatistica2.create_text(self.window_width*0.3,self.window_height*0.15,text="Data:YYYY-MM-DD-HH",font=("verdana",30),fill="white")
        self.data = Entry(self.telaEstatistica2,bg="#6c0090",text="Pesquise aqui",fg="white",font=("verdana",30),bd=5,highlightbackground="black")
        self.data.place(relx=0.30,rely=0.25,anchor="center")
        self.botPesquisar = Button(self.telaEstatistica2,command=lambda:self.enviarPesquisa(self.data,False,self.telaEstatistica2,self.telaSelecao,True),relief="raised",bd=-1,bg="#6c0090",image=self.imgBPesquisaPurple)
        self.botPesquisar.place(relx=0.30,rely=0.80,anchor="center")
        self.botVoltarEstatisca = Button(self.telaEstatistica2,command=lambda:self.mudar(self.telaEstatistica2,self.telaPesquisa),relief="raised",bd=-1,bg="#6c0090",image=self.imgBVoltarAzul)
        self.botVoltarEstatisca.place(relx=0.30,rely=0.93,anchor="center")

    def mudar(self,fechar,abrir):
        fechar.pack_forget()
        abrir.pack()

    def enviarPesquisa(self,caixa,atualizar,fechar,abrir,mostrar):
        self.parametros.append(caixa.get())
        if(atualizar):
            self.mudar(fechar,abrir)
        self.textoPrint = ""
        if(mostrar):
            print(self.teste.execute3keys(self.parametros,queryEstaticaPartida))
            for a in self.parametros:
                self.textoPrint = str(self.textoPrint) + str(a)
            self.show = Label(fechar,fg="black",bd=-1,bg='white',font=("verdana",30),text=self.textoPrint)
            self.show.place(relx=0.8,rely=0.2,anchor="center")
            self.parametros.clear()

    def enviarPesquisa1(self,tipo):
        self.parametros.append(self.caixaTexto.get())
        print(self.teste.execute(self.parametros,tipo))
        



    def admin(self):
        print('Admin')

    def __del__(self):
        print("")
        self.teste.fimQuery()

 

window = Tk()
my_gui = TelaPrincipal(window)


window.mainloop()