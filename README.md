# Trabalho de Base de Dados
Implementação do trabalho proposto na desciplina SSC0640 - Base de Dados.

## Estrutura do Repositório
* **Raiz**: Arquivos relacionados à especificação, relatórios e ao projeto em
geral.
    * **Src**: Diretório contém as pastas com código fonte.
        * **Sql**: Diretório que contém os scripts SQL para o Oracle.
        * **Python**: Diretório contendo o código fonte da Aplicação.

## Integrantes
* Carlos H. L. Melara
* Felipe B. de Oliveira
* Lucas A. de Oliveira
* Lucas N. Camolezi
